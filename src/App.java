import java.util.ArrayList;

import com.devcamp.j04_javabasic.s10.CAnimal;
import com.devcamp.j04_javabasic.s10.CBird;
import com.devcamp.j04_javabasic.s10.CCat;
import com.devcamp.j04_javabasic.s10.CFish;
import com.devcamp.j04_javabasic.s10.CPerson;
import com.devcamp.j04_javabasic.s10.CPet;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        
        CAnimal nameA2 = new CFish();
        CPet name2 = new CBird();
        name2.animalSound();
        name2 = new CFish();
        name2.animalSound();
        name2 = new CCat();
        name2.animalSound();

        CPerson person = new CPerson();
        person.setAge(18);
        ArrayList<CPet> petsList = new ArrayList<>();
        petsList.add(name2);
        petsList.add((CPet)nameA2);
        person.setPets(petsList);
        System.out.println(person);
    }
}
