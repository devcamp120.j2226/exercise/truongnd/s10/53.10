package com.devcamp.j04_javabasic.s10;

public class CBird extends CPet implements IFlyable {
    @Override
    public void fly(){
        System.out.println("Bird flying....");
    };
    protected void print(){
        System.out.println("Bird print....");
    }
    protected void play(){
        System.out.println("Bird play....");
    }
    public static void main(String[] args){
        
    }
}
