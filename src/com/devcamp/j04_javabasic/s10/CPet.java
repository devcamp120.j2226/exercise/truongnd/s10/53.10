package com.devcamp.j04_javabasic.s10;
public class CPet extends CAnimal  {
    protected int age;
    protected String name;
    @Override
    public void animalSound(){
        //TODO Auto-generated method stub
        System.out.println("Pet sound....");
    }
    @Override
    public void eat(){
        System.out.println("Pet eating....");
    };
    protected void print(){
        System.out.println("Pet print....");
    }
    protected void play(){
        System.out.println("Pet play....");
    }
    public static void main(String[] args){
        CPet myPet = new CPet();
        myPet.name = "KIYA";
        myPet.age = 2;
        myPet.animclass = AnimalClass.mammals;
        myPet.eat();
        myPet.animalSound();
        myPet.print();
        myPet.play();

        CFish myFish = new CFish();
        myFish.name = "Gold Fish";
        myFish.animclass = AnimalClass.fish;
        myFish.eat();
        myFish.animalSound();
        myFish.print();
        myFish.play();
        myFish.swim();

        CBird myBird = new CBird();
        myBird.name = "My Eagle";
        myBird.animclass = AnimalClass.birds;
        myBird.eat();
        myBird.animalSound();
        myBird.print();
        myBird.play();
        myBird.fly();

        CCat myCat = new CCat();
        myCat.name = "My Meomeo";
        myCat.animclass = AnimalClass.birds;
        myCat.eat();
        myCat.animalSound();
        myCat.print();
        myCat.play();
        myCat.run();
        myCat.running();

        CDog myDog = new CDog();
        myDog.name = "My Kiki";
        myDog.animclass = AnimalClass.birds;
        myDog.eat();
        myDog.animalSound();
        myDog.print();
        myDog.play();
        myDog.bark();
        myDog.barkBanrk();
        myDog.run();
        myDog.running();
    }
}
