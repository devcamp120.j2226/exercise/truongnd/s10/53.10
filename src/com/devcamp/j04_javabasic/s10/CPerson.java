package com.devcamp.j04_javabasic.s10;

import java.util.ArrayList;

public class CPerson{
    private int id;
    private int age;
    private String firstName;
    private String lastName;
    private ArrayList<CPet> pets;
    public CPerson(){

    }

    public CPerson(int age){
        this.age = age;
    }
    public CPerson(int id, int age, String firstName, String lastName, ArrayList<CPet> pets) {
        this.id = id;
        this.age = age;
        this.firstName = firstName;
        this.lastName = lastName;
        this.pets = pets;
    } 
    public int getAge(){
        return this.age;
    }
    public void setAge(int age){
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public ArrayList<CPet> getPets() {
        return pets;
    }

    public void setPets(ArrayList<CPet> pets) {
        this.pets = pets;
    }
    public void animalSound(){
        System.out.println("Person speaking");
    }
    @Override
    public String toString() {
        return "CPerson {\"id\":" + this.id + ", age= " + age + ", firstName= "
        + firstName + ", lastName= " + lastName + ", pets= " + this.pets + "}";
    }
}


